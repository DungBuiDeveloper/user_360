
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '../bootstrap';
import '../plugins';
import './panolen/script.js';


const menu = document.querySelector('.menu');
const btn = menu.querySelector('.nav-tgl');
btn.addEventListener('click', evt => {
  $('.menu_in').toggleClass('active');

  if (menu.className.indexOf('active') === -1) {
		menu.classList.add('active');

  } else {
		menu.classList.remove('active');
  }
})

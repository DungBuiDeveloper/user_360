
$( document ).ready(function() {

  //Initial Data
  var data ,viewer, container, infospot;
  //Get Data From DB
  if ($('#data').length) {
    setTimeout(function () {
      $('.overloading').fadeOut();
    }, 1500);

    data = JSON.parse($('#data').html());
  }


  //Bar Progess
  var bar = document.querySelector( '#bar' );
  function onProgressUpdate ( event ) {
    var percentage = event.progress.loaded/ event.progress.total * 100;
    bar.style.width = percentage + "%";
    if (percentage >= 100){
      bar.classList.add( 'hide' );
      infospot.position.set(homePo[CurentPano.id][0],homePo[CurentPano.id][1],homePo[CurentPano.id][2]);
      viewer.tweenControlCenter( lookAtPositions[CurentPano.id]);
      setTimeout(function(){
        bar.style.width = 0;
      }, 1000);
    }
  }

  //Load Sider Bar



  //Container Viwer
  container = document.querySelector( '#container' );
  window['panoramaImage'] = [];

  viewer = new PANOLENS.Viewer( { container: container, autoRotate: false, autoRotateSpeed: 0.2, autoRotateActivationDuration: 5000 } );


  $('.title-image').text(data[0].title);

  // SideBar

  var htmlGoBrower = '';
  data.map((item , i)=>{

    let html_header = `<ul>`;

    let html_body = `<li>`;

    if (  i == 0) {
      html_body += `<div class="content-menu title active" data-title="${item.title}" data-id="${item.id}" >

        <a href="javascript:;">
          ${item.title}
        </a>

      </div>`;
    }else {
      html_body += `<div class="content-menu title" data-title="${item.title}" data-id="${item.id}" >

        <a href="javascript:;">
          ${item.title}
        </a>

      </div>`;
    }
    html_body += `</li>`;

    let html_footer = `</ul>`;
    htmlGoBrower += html_header+html_body+html_footer;
  })
  $('#nav_side').empty();
  $('#nav_side').append(htmlGoBrower);
  var homePo = [
    [4978.89, -142.44, 296.05],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
    [4837.88, -232.63, -1201.57],
  ];

  var lookAtPositions = [
    new THREE.Vector3(4978.89, -142.44, 296.05),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
    new THREE.Vector3(4837.88, -232.63, -1201.57),
  ];

  PANOLENS.Viewer.prototype.getPosition = function () {
	var intersects, point, panoramaWorldPosition, outputPosition;
	intersects = this.raycaster.intersectObject( this.panorama, true );

	if ( intersects.length > 0 ) {
		point = intersects[0].point;
		panoramaWorldPosition = this.panorama.getWorldPosition();

		// Panorama is scaled -1 on X axis
		outputPosition = new THREE.Vector3(
			-(point.x - panoramaWorldPosition.x).toFixed(2),
			(point.y - panoramaWorldPosition.y).toFixed(2),
			(point.z - panoramaWorldPosition.z).toFixed(2)
		);
	}

	  return [-(point.x - panoramaWorldPosition.x).toFixed(2) , Number((point.y - panoramaWorldPosition.y).toFixed(2)) , Number((point.z - panoramaWorldPosition.z).toFixed(2)) ];
  };

  data.map((item,i)=>{
    window['panoramaImage'].push(item);
  })



  for (var i = 0; i < window['panoramaImage'].length; i++) {
    let item = window['panoramaImage'][i];
    window[`panoramaImage${item.id}`] = new PANOLENS.ImagePanorama( window['panoramaImage'][i].image );
    window[`panoramaImagePlanet${item.id}`] = new PANOLENS.ImageLittlePlanet( window['panoramaImage'][i].image , 20000 );
    window[`panoramaImage${item.id}`].addEventListener( 'progress', onProgressUpdate );
    infospot = new PANOLENS.Infospot( 100, PANOLENS.DataImage.Info );
    window[`panoramaImage${item.id}`].add( infospot );
    viewer.add( window[`panoramaImage${item.id}`] );
  }
  infospot.position.set(homePo[0][0],homePo[0][1],homePo[0][2]);
  viewer.tweenControlCenter( lookAtPositions[0]);

  // Enter panorama when load completes
  function changePano( targetPanorama ) {
    bar.classList.remove( 'hide' );
    fov = 120;
    viewer.setPanorama( window[`panoramaImage${targetPanorama}`] );
  }


  var GoCenter = {
    title:'Tới Điểm Bắt Đầu',

    style: {
      width:'30px',
      backgroundImage: 'url(/panolen/home.png)',
      float: 'right'
    },

    onTap: function(){

      infospot.focus();
    }
  };
  var rotateAuto = true;
  var Rotate = {
    title:'Tự Xoay',

    style: {
      width:'30px',
      backgroundImage: 'url(/panolen/rotate.png)',
      float: 'right'
    },

    onTap: function(){
      rotateAuto = !rotateAuto;
      if (rotateAuto) {
        viewer.enableAutoRate();
      }else {
        viewer.disableAutoRate();
      }
    }
  };
  var map = false;
  var Map = {
    title:'Bản Đồ',

    style: {
      width:'25px',
      backgroundImage: 'url(/panolen/marker.png)',
      float: 'right'
    },

    onTap: function(){
      map = !map;
      if (map) {
        $('.map').show();
        $('.left_site').hide();
      }else {
        $('.map').hide();
        $('.left_site').show();
      }
    }
  };
  var fov = 100;
  var ZoomIn = {
    title:'Zoom Vào',

    style: {
      width:'25px',
      backgroundImage: 'url(/panolen/zoom_in.png)',
      float: 'right'
    },

    onTap: function(){
      if (fov < 120) {
        fov += 10;
        viewer.setCameraFov(fov);

      }

    }
  };
  var ZoomOut = {
    title:'Zoom Ra',

    style: {
      width:'25px',
      backgroundImage: 'url(/panolen/zoom_out.png)',
      float: 'right'
    },

    onTap: function(){
      if (fov > 20) {
        fov -= 10;
        viewer.setCameraFov(fov);
      }
    }
  };

  var Audio = {
    title:'Tắt Bật Nhạc',
    style: {
      backgroundImage: 'url(/panolen/play.png)',
      width:'25px',
      float: 'right'
    },

    onTap: function(){
      var myAudio = document.getElementById("myAudio");

      if (myAudio.duration > 0 && !myAudio.paused) {
        $(this).css('background-image','url(/panolen/play.png)');
        myAudio.pause();

      } else {
        $(this).css('background-image','url(/panolen/pause.png)');
        myAudio.play();

      }


    }
  };

  var CurentPano = window['panoramaImage'][0];
  function getNext() {
    for (var i = 0; i < window['panoramaImage'].length; i++) {
      if (CurentPano.id == window['panoramaImage'][i].id) {

        if (i+1 < window['panoramaImage'].length) {
          CurentPano = window['panoramaImage'][i+1];
        }else {
          CurentPano = window['panoramaImage'][0];
        }

        return true;
      }
    }
  }
  function getPrev() {
    for (var i = 0; i < window['panoramaImage'].length; i++) {
      if (CurentPano.id == window['panoramaImage'][i].id) {

        if (i-1 == -1) {
          CurentPano = window['panoramaImage'][window['panoramaImage'].length];
        }else {
          CurentPano = window['panoramaImage'][i - 1];
        }

        return true;
      }
    }
  }
  var Prev = {
    title:'Lui View Sau',
    style: {
      backgroundImage: 'url(/panolen/prev.png)',
      width:'25px',
      float: 'right'
    },

    onTap: function(){
      getPrev();
      changePano(CurentPano.id);
      $('.title-image').text(CurentPano.title);
    }
  };
  var Next = {
    title:'Tới View Tiếp Theo',
    style: {
      backgroundImage: 'url(/panolen/next.png)',
      width:'25px',
      float: 'right'
    },

    onTap: function(){
      getNext();
      changePano(CurentPano.id);
      $('.title-image').text(CurentPano.title);
    }
  };
  function myTimerSlider() {
    getNext();
    changePano(CurentPano.id);
    $('.title-image').text(CurentPano.title);
  }
  var TimeIn;
  var Slider = {
    title:'Trình Diễn',
    style: {
      backgroundImage: 'url(/panolen/slider.png)',
      width:'25px',
      float: 'right'
    },

    onTap: function(){

      let html = `<span style="display:block;width:100%;text-alight:center">Bắt Đầu Trình Diễn</span><span style="display:block;width:100%;text-alight:center">Click Stop thoát khỏi chế độ trình diễn</span>`;
      $('.alertbar').empty();

      $('.alertbar').append(html);
      $('.alertbar').fadeIn();
      $('#toogle_footer_bar').addClass('active');
      viewer.toggleControlBar();
      setTimeout(function () {
        infospot.focus();
        $('.alertbar').fadeOut();
      }, 3000);
      TimeIn = setInterval(myTimerSlider, 30000);

    }
  };

  $('#stop_slider').bind("click touchstart", function() {
    clearInterval(TimeIn);
    viewer.toggleControlBar();
    $('#toogle_footer_bar').removeClass('active');

   });
  var globalStatus = false;

  var global = {
    title:'Toàn Cầu',
    style: {
      backgroundImage: 'url(/panolen/global.png)',
      width:'25px',
      float: 'right'
    },

    onTap: function(){
      globalStatus = !globalStatus;
      if (globalStatus) {
        viewer.add( window[`panoramaImagePlanet${CurentPano.id}`] );
        viewer.setPanorama( window[`panoramaImagePlanet${CurentPano.id}`] );
        viewer.animate();

      }else {
        infospot.position.set(homePo[CurentPano.id][0],homePo[CurentPano.id][1],homePo[CurentPano.id][2]);
        viewer.setPanorama( window[`panoramaImage${CurentPano.id}`] );
        setTimeout(function () {
          viewer.tweenControlCenter( lookAtPositions[CurentPano.id]);
        }, 300);

      }
    }
  };



  viewer.appendControlItem(Audio);
  viewer.appendControlItem(GoCenter);
  viewer.appendControlItem(Rotate);
  viewer.appendControlItem(Map);
  viewer.appendControlItem(ZoomIn);
  viewer.appendControlItem(ZoomOut);
  viewer.appendControlItem(Next);
  viewer.appendControlItem(Prev);
  viewer.appendControlItem(Slider);
  viewer.appendControlItem(global);
  viewer.setCameraFov(120);

  //
  // $('#container').click(function() {
  //   console.log(viewer.getPosition());
  // });


  // $("#toogle_footer_bar").on('click', function (event) {
  //
  //   viewer.toggleControlBar();
  //   $(this).toggleClass("active");
  // });


   $('#toogle_footer_bar').bind("click touchstart", function() {
     viewer.toggleControlBar();
     $(this).toggleClass("active");

    });




//Show Hide Simple


  $('.hint').click(function() {
    $(this).fadeOut();
    var x = document.getElementById("myAudio");
    x.play();
  })


  $('.close_side_bar').click(function() {
    $('.left_site').animate({"left": '-=500'});
    $('.show_side_bar').show();
  });

  $('.show_side_bar').click(function() {
    $('.left_site').animate({"left": '0'});
    $('.show_side_bar').hide();
  });






  $('.to-home').click(function () {
    infospot.position.set( 0, 0, 0 );
  })

  $(document).on("click",".content-menu.title",function() {

    event.stopPropagation();
    let idImagePano = $(this).attr('data-id');
    let titleImagePano = $(this).attr('data-title');
    $('.content-menu.title').removeClass('active');
    $(this).addClass('active');
    changePano(idImagePano);
    $('.title-image').text(titleImagePano);
    for (var i = 0; i < window['panoramaImage'].length; i++) {
      if (idImagePano == window['panoramaImage'][i].id) {
        CurentPano = window['panoramaImage'][i];
        return true;
      }
    }


  });






  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

    $('.toogle_menu').bind("touchstart", function(event) {

        event.stopPropagation();
        let db = {};
        db.data = {animateIn: "plusButton", animateOut: "closeButton"};

        animate_function2(db);
     });




    function animate_function2(event){


      $('#nav_side').slideToggle();


        if( $('.btn-round').hasClass(event.data.animateIn) ) {
              $('.btn-round').removeClass(event.data.animateIn).addClass(event.data.animateOut);
             }
          else if( $('.btn-round').hasClass(event.data.animateOut) ) {
             $('.btn-round').removeClass(event.data.animateOut).addClass(event.data.animateIn);
          }
          else {
            $('.btn-round').addClass('animated ' + event.data.animateIn);
          }
    }
  }else {
    $(".toogle_menu").click({animateIn: "plusButton", animateOut: "closeButton"}, animate_function);


    function animate_function(event){

      event.stopPropagation();
      console.log("crash 23");
      $('#nav_side').slideToggle();


        if( $('.btn-round').hasClass(event.data.animateIn) ) {
              $('.btn-round').removeClass(event.data.animateIn).addClass(event.data.animateOut);
             }
          else if( $('.btn-round').hasClass(event.data.animateOut) ) {
             $('.btn-round').removeClass(event.data.animateOut).addClass(event.data.animateIn);
          }
          else {
            $('.btn-round').addClass('animated ' + event.data.animateIn);
          }
    }
  }











});

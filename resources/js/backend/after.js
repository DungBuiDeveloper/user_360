// Loaded after CoreUI app.js
import '../lang_javascript';
import './editor';
import './category';
import './tag';
import './media';
import './post';
import './user_customer';

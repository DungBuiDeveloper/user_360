
import addDeleteForms from "../plugins";
$(function () {
    if ($('#user_customer_table').length) {
        $('#user_customer_table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": '/admin/user_customers/ajaxData',
                "type": "POST"
            },
            "language": {
                "url": `${baseUrl}/json/`+langDataTable
            },
            "lengthMenu": [ 5, 10, 15, 25, 30 ],
            "columns": [
                { data: "id" , orderable: false},
                { data: "name" },
                { data: "email" },
                { data: "created_at" },
                { data: "updated_at"},
                { data: "action" , orderable: false},

            ],
            "order": [[ 1, "desc" ]],
            "drawCallback": ( settings ) => {
                addDeleteForms();
            }
        });
    }


    if ($('#user_customer_add_edit').length) {

        //Validate
        $("#user_customer_add_edit").validate({
            errorClass: "alert alert-danger",
            errorElement: "div",
            rules:{
                name:'required',
                email:'required',
                password:'required',
            }
        });

        //Click get image for thumb
        $(document).on("click",".get_info_iamge",function() {
            let data = JSON.parse($(this).find('.info').html());
            let preview = $(this).find('img').attr('src');
            $('#thumbnail').val(data.id);
            preview = `<img src="${preview}" alt="preview image" />`;
            $('.preview_image').empty();
            $('.preview_image').append(preview);
            $("#imagelistModal .close").click();
        });

        //Dropzone
        new Dropzone('#user_customer_add_edit', {

            autoProcessQueue: true,
            uploadMultiple: false,
            url:'/admin/media/storeOnlyImage',
            acceptedFiles:"image/*",
            paramName: "files",
            parallelUploads: 100,
            previewsContainer:".dropzone-previews",
            addRemoveLinks: true,
            clickable: ".needsclick",
            maxFiles: 1,
            sending: function(file, xhr, formData) {
                formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
            },
            init: function() {
              var myDropzone = this;

              // First change the button to actually tell Dropzone to process the queue.
             document.querySelector('button[type="submit"]').addEventListener("click", function(e) {
                // Make sure that the form isn't actually being sent.
                if (myDropzone.files.length !== 0) {

                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                }

              });

              // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
              // of the sending event because uploadMultiple is set to true.
              this.on("sendingmultiple", function() {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.

              });
              this.on("success", function(files, response) {
                $('#thumbnail').val(response.id_image);
                myDropzone.removeFile(files);
                $('.preview_image').empty();
                $('.preview_image').append(`<img src="${response.url_image}" alt="preview image">`);

              });
              this.on("errormultiple", function(files, response) {
                console.log(response);
              });
              this.on("dragover", function() {
                $('.needsclick').addClass('borderOver');
              });
              this.on("dragleave", function() {
                $('.needsclick').removeClass('borderOver');
              });
              this.on("addedfile", function() {
                $('.needsclick').addClass('addedfile');
              });
              this.on("removedfile", function(file) {

                if (myDropzone.files.length === 0) {
                    $('.needsclick').removeClass('addedfile');
                }

              });

            }
      });
    }

});

@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
  <i class="icono-home" aria-hidden="true"></i>
  <audio  style="height:0;z-index:-1;position: absolute;" id="myAudio" controls autoplay>
  <source src="https://threejs.org/examples/sounds/358232_j_s_song.mp3" type="audio/mpeg">
  </audio>
  <div id="progress">
    <div id="bar"></div>
  </div>
  <div id="container" >





    <div style="display:none" class="alertbar">
      ádasdasd
    </div>

    <div class="map">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3826.228597660818!2d107.59244401537055!3d16.463958588638114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a13e81a48041%3A0x4a7f593b30eeb5de!2zNzggQuG6v24gTmdow6ksIFBow7ogSOG7mWksIFRow6BuaCBwaOG7kSBIdeG6vywgVGjhu6thIFRoacOqbiBIdeG6vywgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1589135790468!5m2!1sen!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>


    <div class="menu"><button class="nav-tgl" type="button" aria-label="toggle menu"><!-- this span just for the three dividers in the hamburger button--><span aria-hidden="true"></span></button>
        <nav class="nav">
            <!-- I don't care about the menu elements here so I will hide them-->
            <ul class="menu_in">
                <li><a href="/">Tổng Thể</a></li>
                <li><a href="/tien-ich">Tiện ích</a></li>
                <li><a href="/noi-that">Nội Thất</a></li>

            </ul>
        </nav>
    </div>



    <div class="left_site">
      <a class="toogle_menu" href="javascript:;">
        <div class="btn-round animated closeButton">
          <span class="close"></span>
        </div>
        SKY OASIS
      </a>
      <div id="nav_side" class="nav" ></div>

    </div>


  <p class="title-image">
    title will be replace
  </p>
  <a id="toogle_footer_bar" href="javascript:;">
    <i class="fa fa-eye" aria-hidden="true"></i>
    <i class="fa fa-eye-slash eye2" aria-hidden="true"></i>
  </a>
  <a id="stop_slider" href="javascript:;"><i class="fa fa-stop-circle" aria-hidden="true"></i></a>
  </div>
  <div style="display:none" id="data">
    @php
      echo json_encode($dataImage);
    @endphp
  </div>
@endsection

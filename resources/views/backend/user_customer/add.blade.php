@extends('backend.layouts.general')
@section('title', __('labels.backend.access.user_customer.management') . ' | ' . __('labels.backend.access.user_customer.edit'))
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.user_customer.management')
                        <small class="text-muted">@lang('labels.backend.access.user_customer.create')</small>
                    </h4>
                </div><!--col-->
            </div>
        </div>
        <div class="card-body">
            <div class="row">

                <div class="col-sm-12">
                    <form id="user_customer_add_edit" method="POST" action={{route('admin.user_customers.add')}}>
                        @csrf
                        <div class="form-group">
                            <label for="name">@lang('labels.backend.access.user_customer.table.name')</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="@lang('labels.backend.access.user_customer.table.name')" value="{{ old('name') }}">
                        </div>
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="email">@lang('labels.backend.access.user_customer.table.email')</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="@lang('labels.backend.access.user_customer.table.email')" value="{{ old('email') }}">
                        </div>
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="password">@lang('labels.backend.access.user_customer.table.password')</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="@lang('labels.backend.access.user_customer.table.password')" value="{{ old('password') }}">
                        </div>
                        @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="password_confirmation">@lang('labels.backend.access.user_customer.table.password_confirmation')</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="@lang('labels.backend.access.user_customer.table.password_confirmation')" value="{{ old('password_confirmation') }}">
                        </div>
                        @error('password_confirmation')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="form-group">
                            <input type="hidden" value="image" name="type_thumb" id="type_thumb" />
                            <div id="image_thumbnail" role="tabpanel" class="tab-pane fade in active show">

                                <input type="hidden" class="form-control" id="thumbnail" name="thumbnail" placeholder="@lang('labels.backend.access.post.table.thumbnail')" value="{{ old('thumbnail') }}">

                                <div class="dz-clickable dz-message dz-preview dz-image-preview needsclick">
                                    <i class="fa fa-camera" aria-hidden="true"></i>
                                    <div class="dropzone-previews dropzone"></div>
                                </div>

                                <a href="javascript:;" type="button"  data-toggle="modal" data-target="#imagelistModal">
                                    @lang('labels.backend.access.post.thumbnailTitle')
                                </a>

                                <div class="preview_image"></div>
                            </div>
                            {{-- Image --}}
                        </div>
                        {{-- Thumnail --}}
                        <button type="submit" class="btn btn-primary">@lang('buttons.general.submit')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="list-image-modal">
        <div class="modal fade" id="imagelistModal" tabindex="-1" role="dialog" aria-labelledby="imagelistModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="imagelistModalLabel">@lang('labels.backend.access.post.modal_title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                @include('backend.includes.modal_list_image')
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('buttons.general.cancel')</button>
              </div>
            </div>
          </div>
        </div>
    </div>
    {{-- List Image Modal --}}
@endsection

@extends('backend.layouts.general')

@section('title', __('labels.backend.access.user_customer.management') . ' | ' . __('labels.backend.access.user_customer.edit'))
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.user_customer.management')
                        <small class="text-muted">@lang('labels.backend.access.user_customer.edit')</small>
                    </h4>
                </div><!--col-->
                <div class="col-sm-12">
                    <form method="POST" id="user_customer_add_edit"  action={{route('admin.user_customers.edit')}}>
                        @csrf
                        <input type="hidden" class="form-control" name="id"  value="{{ $userCustomer->id }}">
                        <div class="form-group">
                            <label for="name">@lang('labels.backend.access.user_customer.table.name')</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $userCustomer->name }}">
                        </div>
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="email">@lang('labels.backend.access.user_customer.table.email')</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $userCustomer->email }}">
                        </div>
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <input type="hidden" value="image" name="type_thumb" id="type_thumb" />
                            <div id="image_thumbnail" role="tabpanel" class="tab-pane fade in active show">

                                <input type="hidden" class="form-control" id="thumbnail" name="thumbnail" placeholder="@lang('labels.backend.access.post.table.thumbnail')" value="{{ $userCustomer->thumbnail }}">

                                <div class="dz-clickable dz-message dz-preview dz-image-preview needsclick">
                                    <i class="fa fa-camera" aria-hidden="true"></i>
                                    <div class="dropzone-previews dropzone"></div>
                                </div>

                                <a href="javascript:;" type="button"  data-toggle="modal" data-target="#imagelistModal">
                                    @lang('labels.backend.access.post.thumbnailTitle')
                                </a>

                                <?php if($userCustomer->getThumbnail !== null){ ?>
                                  <div class="preview_image">
                                    <img src="{{ $userCustomer->getThumbnail->getUrl() }}" alt="">
                                  </div>
                                <?php } else { ?>
                                  <div class="preview_image">
                                    <img src="https://fakeimg.pl/250x100/" alt="">
                                  </div>
                                <?php } ?>
                            </div>
                            {{-- Image --}}
                        </div>
                        {{-- Thumnail --}}
                        <button type="submit" class="btn btn-primary">@lang('buttons.general.submit')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="list-image-modal">
        <div class="modal fade" id="imagelistModal" tabindex="-1" role="dialog" aria-labelledby="imagelistModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="imagelistModalLabel">@lang('labels.backend.access.post.modal_title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                @include('backend.includes.modal_list_image')
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('buttons.general.cancel')</button>
              </div>
            </div>
          </div>
        </div>
    </div>
    {{-- List Image Modal --}}
@endsection

@extends('backend.layouts.general')

@section('title', __('labels.backend.access.user_customer.management') . ' | ' . __('labels.backend.access.user_customer.detail'))
@section('content')
    <div class="card">
      <div class="card-header">
          <div class="row">
              <div class="col-sm-5">
                  <h4 class="card-title mb-0">
                      @lang('labels.backend.access.user_customer.management')
                      <small class="text-muted">@lang('labels.backend.access.user_customer.detail')</small>
                  </h4>
              </div><!--col-->
              <div class="col-sm-7 text-right">
                  <a href="{{ route('admin.user_customers.showFormEditChangePass', $userCustomer->id) }}" class="btn btn-success">Change Password</a>
              </div>
          </div>
      </div>
        <div class="card-body">
          <div class="container">
            <div class="form-group">
              <div class="row">
                <div class="col">
                  @lang('labels.backend.access.user_customer.table.name')
                </div>
                <div class="col">
                  {{$userCustomer->name}}
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col">
                  @lang('labels.backend.access.user_customer.table.email')
                </div>
                <div class="col">
                  {{$userCustomer->email}}
                </div>
              </div>
            </div>
            <?php if($userCustomer->getThumbnail !== null){ ?>
              <div class="form-group">
                <div class="row">
                  <div class="col">
                    @lang('labels.backend.access.user_customer.table.thumbnail')
                  </div>
                  <div class="col">
                      <img src="{{ $userCustomer->getThumbnail->getUrl() }}" alt="">
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
    </div>
@endsection

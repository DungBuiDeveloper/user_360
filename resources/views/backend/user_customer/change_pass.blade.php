@extends('backend.layouts.general')

@section('title', __('labels.backend.access.user_customer.management') . ' | ' . __('labels.backend.access.user_customer.change_pass'))
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.user_customer.management')
                        <small class="text-muted">@lang('labels.backend.access.user_customer.change_pass')</small>
                    </h4>
                </div><!--col-->
                <div class="col-sm-12">
                    <form method="POST" id="user_customer_add_edit"  action={{route('admin.user_customers.change_pass')}}>
                        @csrf
                        <input type="hidden" class="form-control" name="id"  value="{{ $userCustomer->id }}">
                        <div class="form-group">
                            <label for="password">@lang('labels.backend.access.user_customer.table.password')</label>
                            <input type="password" class="form-control" id="password" name="password" value="">
                        </div>
                        @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="password_confirmation">@lang('labels.backend.access.user_customer.table.password_confirmation')</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="{{ $userCustomer->password_confirmation }}">
                        </div>
                        @error('password_confirmation')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="submit" class="btn btn-primary">@lang('buttons.general.submit')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

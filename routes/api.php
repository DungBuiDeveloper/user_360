<?php

use Dingo\Api\Routing\Router;

$api = app(Router::class);

$api->version('v1', ['prefix' => 'api/v1', 'namespace' => 'App\Api\V1\Controllers'], function (Router $api) {
    $api->group(['prefix' => 'auth'], function (Router $api) {
        $api->post('signup', 'SignUpController@signUp');
        $api->post('login', 'LoginController@login');

        $api->post('recovery', 'ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'ResetPasswordController@resetPassword');

        $api->post('logout', 'LogoutController@logout');
        $api->post('refresh', 'RefreshController@refresh');
        $api->post('change_password', 'UserController@changePassword');
        $api->get('me', 'UserController@me');
    });

    $api->group(['middleware' => ['auth:api']], function (Router $api) {
        $api->get('protected', function () {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.',
            ]);
        });
        $api->get('render_token', 'UserController@renderToken');

        //HOME SCREEN
        $api->get('initialDataHome', 'HomeController@index');
        $api->get('events', 'HomeController@events');

        //Product Screen
        $api->get('products', 'ProductController@index');
        $api->get('products/{id}', 'ProductController@getProductDetail');
        $api->post('postExample', 'HomeController@postExample');
    });
    $api->post('upload_avatar', 'MediaLibraryController@upload_avatar');
    $api->post('signature', 'MediaLibraryController@storeSignature');
});

<?php

Breadcrumbs::for('admin.user_customers.list', function ($trail) {
    $trail->push(__('labels.backend.access.user_customer.management'), route('admin.user_customers.list'));
});

Breadcrumbs::for('admin.user_customers.showFormAdd', function ($trail) {
    $trail->parent('admin.user_customers.list');
    $trail->push(__('menus.backend.access.user_customer.create'), route('admin.user_customers.showFormAdd'));
});

Breadcrumbs::for('admin.user_customers.showFormEdit', function ($trail, $id) {
    $trail->parent('admin.user_customers.list');
    $trail->push(__('menus.backend.access.user_customer.edit'), route('admin.user_customers.showFormEdit', $id));
});

Breadcrumbs::for('admin.user_customers.detail', function ($trail, $id) {
    $trail->parent('admin.user_customers.list');
    $trail->push(__('menus.backend.access.user_customer.detail'), route('admin.user_customers.detail', $id));
});

Breadcrumbs::for('admin.user_customers.showFormEditChangePass', function ($trail, $id) {
    $trail->parent('admin.user_customers.list');
    $trail->push(__('menus.backend.access.user_customer.change_pass'), route('admin.user_customers.showFormEditChangePass', $id));
});

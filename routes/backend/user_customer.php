<?php

use App\Http\Controllers\Backend\Auth\User\UserCustomerController;

// All route names are prefixed with 'admin.'.
Route::get('user_customers', [UserCustomerController::class, 'index'])->name('user_customers.list');
Route::get('user_customers/{id}/detail', [UserCustomerController::class, 'detail'])->name('user_customers.detail');
Route::get('user_customers/{id}/change_pass', [UserCustomerController::class, 'showFormEditPass'])->name('user_customers.showFormEditChangePass');
Route::get('user_customers/store', [UserCustomerController::class, 'showFormAdd'])->name('user_customers.showFormAdd');
Route::get('user_customers/{id}/edit', [UserCustomerController::class, 'showFormEdit'])->name('user_customers.showFormEdit');
Route::post('user_customers/add', [UserCustomerController::class, 'storeUserCustomer'])->name('user_customers.add');
Route::post('user_customers/edit', [UserCustomerController::class, 'editUserCustomer'])->name('user_customers.edit');
Route::post('user_customers/change_pass', [UserCustomerController::class, 'editPassUserCustomer'])->name('user_customers.change_pass');
Route::delete('user_customers/destroy', [UserCustomerController::class, 'destroy'])->name('user_customers.destroy');
Route::post('user_customers/ajaxData', [UserCustomerController::class, 'ajaxDataTable'])->name('user_customers.ajax');

<?php
use Illuminate\Database\Seeder;
use App\Models\BackEnd\Media;

use App\Models\Auth\User_customer;



class UserCustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      DB::table('user_customers')->truncate();

  

      factory(User_customer::class,100)->create();



    }
}

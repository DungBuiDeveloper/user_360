<?php

use Illuminate\Database\Seeder;
use App\Models\BackEnd\MediaLibrary;
class Media extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MediaLibrary::firstOrCreate([]);
    }
}

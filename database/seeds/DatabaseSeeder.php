<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use App\Models\BackEnd\MediaLibrary;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Seed the application's database.
     */
    public function run()
    {
        Model::unguard();

        MediaLibrary::firstOrCreate([]);

        $this->truncateMultiple([
            'cache',
            'ledgers',
            'sessions',
        ]);

        // $this->call(AuthTableSeeder::class);
        $this->call([
          AuthTableSeeder::class
        ]);

        Model::reguard();
    }
}

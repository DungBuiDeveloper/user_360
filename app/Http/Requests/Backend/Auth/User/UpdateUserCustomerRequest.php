<?php

namespace App\Http\Requests\Backend\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|max:255|unique:user_customers,email,'.$request->id,
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A name is required',
            'name.max' => 'A name is max 255',
            'email.required' => 'A email is required',
            'email.max' => 'A email is max 255',
            'email.unique' => 'A email is unique',
        ];
    }
}

<?php

namespace App\Http\Requests\Backend\Auth\User;

use Illuminate\Foundation\Http\FormRequest;

class UserCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'GET') {
            return [];
        }

        return [
            'name' => 'required|max:255',
            'email' => 'required|max:255|unique:user_customers',
            'password' => 'required|min:6|max:30|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A name is required',
            'name.max' => 'A name is max 255',
            'email.required' => 'A email is required',
            'email.max' => 'A email is max 255',
            'email.unique' => 'A email is unique',
            'password.required' => 'A password is required',
            'password.min' => 'A password is 6 -> 30',
            'password.max' => 'A password is 6 -> 30',
            'password.confirmed' => 'Your password and confirmation password do not match',
        ];
    }
}

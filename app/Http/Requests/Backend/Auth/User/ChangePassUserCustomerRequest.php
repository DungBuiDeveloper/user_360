<?php

namespace App\Http\Requests\Backend\Auth\User;

use Illuminate\Foundation\Http\FormRequest;

class ChangePassUserCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:6|max:30|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'A password is required',
            'password.min' => 'A password is 6 -> 30',
            'password.max' => 'A password is 6 -> 30',
            'password.confirmed' => 'Your password and confirmation password do not match',
        ];
    }
}

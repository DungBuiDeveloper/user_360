<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\MediaRepository;
use App\Repositories\Backend\UserCustomerRepository;
use App\Http\Requests\Backend\Auth\User\UserCustomerRequest;
use App\Http\Requests\Backend\Auth\User\UpdateUserCustomerRequest;
use App\Http\Requests\Backend\Auth\User\ChangePassUserCustomerRequest;

class UserCustomerController extends Controller
{
    public function __construct(UserCustomerRepository $UserCustomerRepository, MediaRepository $MediaRepository)
    {
        $this->UserCustomerRepository = $UserCustomerRepository;
        $this->MediaRepository = $MediaRepository;
    }

    public function ajaxDataTable()
    {
        return $this->UserCustomerRepository->getAjaxDataTable(null);
    }

    public function index()
    {
        return view('backend/user_customer/index');
    }

    /**
     * [showFormAdd Show Form].
     * @return [Layout] [Return View Add form]
     */
    public function showFormAdd(UserCustomerRequest $request)
    {
        // Init Data Post
        $allImage = $this->MediaRepository->getMediaManager();
        // Ajax Return View Data
        if ($request->ajax()) {
            return view('backend/includes/modal_list_image')->withAllImage($allImage);
        }

        return view('backend/user_customer/add')->withAllImage($allImage);
    }

    /**
     * [storeuser_customer Save user_customer].
     */
    public function storeUserCustomer(UserCustomerRequest $request)
    {
        $data = $request->All();

        // if ($data['thumbnail'] === null) {
        //     $data['thumbnail'] = 0;
        // }

        $save = $this->UserCustomerRepository->storeUserCustomer($data);

        if ($save) {
            return redirect()->route('admin.user_customers.list')->withFlashSuccess(__('alerts.backend.user_customer.created'));
        }

        return redirect()->back()->withFlashDanger(__('alerts.backend.user_customer.created_fail'));
    }

    /**
     * [showFormEdit show Form Edit].
     * @param  string $slug [Unique String Get UserCustomer]
     * @return [type] [Return View Edit form]
     */
    public function showFormEdit($id = '')
    {
        $UserCustomer = $this->UserCustomerRepository->getUserCustomerById($id);

        // Init Data Post
        $allImage = $this->MediaRepository->getMediaManager();

        if (! $UserCustomer) {
            return redirect()->route('admin.user_customers.list')->withFlashDanger(__('alerts.backend.user_customer.not_found'));
        }

        return view('backend/user_customer/edit')
            ->withAllImage($allImage)
            ->withUserCustomer($UserCustomer);
    }

    /**
     * [showFormEdit show Form Edit].
     * @param  string $slug [Unique String Get UserCustomer]
     * @return [type] [Return View Edit form]
     */
    public function showFormEditPass($id = '')
    {
        $UserCustomer = $this->UserCustomerRepository->getUserCustomerById($id);

        if (! $UserCustomer) {
            return redirect()->route('admin.user_customers.list')->withFlashDanger(__('alerts.backend.user_customer.not_found'));
        }

        return view('backend/user_customer/change_pass')
            ->withUserCustomer($UserCustomer);
    }

    /**
     * [editCategory Put Category].
     */
    public function editPassUserCustomer(ChangePassUserCustomerRequest $request)
    {
        $data = $request->only('id', 'password', 'password_confirmation');
        $edit = $this->UserCustomerRepository->editUserCustomer($data);

        if (isset($edit) && $edit) {
            return redirect()->route('admin.user_customers.list')->withFlashSuccess(__('alerts.backend.user_customer.created'));
        }

        return redirect()->back()->withFlashDanger(__('alerts.backend.user_customer.created_fail'));
    }

    /**
     * [editCategory Put Category].
     */
    public function editUserCustomer(UpdateUserCustomerRequest $request)
    {
        $data = $request->all();
        $edit = $this->UserCustomerRepository->editUserCustomer($data);

        if (isset($edit) && $edit) {
            return redirect()->route('admin.user_customers.list')->withFlashSuccess(__('alerts.backend.user_customer.updated'));
        }

        return redirect()->back()->withFlashDanger(__('alerts.backend.user_customer.updated_fail'));
    }

    /**
     * [destroy Delete UserCustomer].
     * @param  string $id [Get UserCustomer]
     */
    public function destroy()
    {
        $delete = $this->UserCustomerRepository->destroy($_GET['id']);

        if ($delete) {
            return redirect()->route('admin.user_customers.list')->withFlashSuccess(__('alerts.backend.user_customer.deleted'));
        }

        return redirect()->route('admin.user_customers.list')->withFlashSuccess(__('alerts.backend.user_customer.deleted_fail'));
    }

    /**
     * [detail show category detail].
     * @param  [string] $slug [unique condition for get category]
     * @return [Object]       [Category Detail]
     */
    public function detail($id)
    {
        $userCustomer = $this->UserCustomerRepository->getUserCustomerById($id);

        if (! $userCustomer) {
            return redirect()->route('admin.user_customers.list')->withFlashDanger(__('alerts.backend.user_customer.not_found'));
        }

        return view('backend/user_customer/detail')->withUserCustomer($userCustomer);
    }
}

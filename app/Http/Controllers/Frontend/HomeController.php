<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
      $dataImage = [
        [
          'image' =>  '/panolen/src/tt/pa1.jpg',
          'title'=>'Tổng thể view biệt thự đảo',
          'id' => 0
        ],
        [
          'image' =>  '/panolen/src/tt/pa2.jpg',
          'title'=>'Tổng thể view sông hồng',
          'id' => 1
        ]

      ];

      return view('frontend.index')->withDataImage($dataImage);
    }

    public function service()
    {
      $dataImage = [
        [
          'image' =>  '/panolen/src/ser/pa1.jpg',
          'title'=>'Hồ bơi chân mây',
          'id' => 0
        ],
        [
          'image' =>  '/panolen/src/ser/pa2.jpg',
          'title'=>'Hồ bơi tầng 3 và resort',
          'id' => 1
        ],
        [
          'image' =>  '/panolen/src/ser/pa3.jpg',
          'title'=>'Khu raining zone',
          'id' => 2
        ],
        [
          'image' =>  '/panolen/src/ser/pa4.jpg',
          'title'=>'Khu shop house',
          'id' => 3
        ],
        [
          'image' =>  '/panolen/src/ser/pa5.jpg',
          'title'=>'Phố đi bộ dài 2.5 km',
          'id' => 4
        ],
        [
          'image' =>  '/panolen/src/ser/pa6.jpg',
          'title'=>'Vườn dạo bộ',
          'id' => 5
        ],
        [
          'image' =>  '/panolen/src/ser/pa7.jpg',
          'title'=>'Vườn thượng uyển',
          'id' => 6
        ],


      ];

      return view('frontend.index')->withDataImage($dataImage);
    }

    public function house()
    {
      $dataImage = [
        [
          'image' =>  '/panolen/src/house/pa1.jpg',
          'title'=>'Ban công view sông Hồng',
          'id' => 0
        ],
        [
          'image' =>  '/panolen/src/house/pa2.jpg',
          'title'=>'Ban công view vịnh đảo',
          'id' => 1
        ],
        [
          'image' =>  '/panolen/src/house/pa3.jpg',
          'title'=>'Căn 2 PN - phòng khách',
          'id' => 2
        ],
        [
          'image' =>  '/panolen/src/house/pa4.jpg',
          'title'=>'Căn 2 PN - phòng ngủ con',
          'id' => 3
        ],
        [
          'image' =>  '/panolen/src/house/pa5.jpg',
          'title'=>'Căn 2 PN - phòng ngủ master',
          'id' => 4
        ],
        [
          'image' =>  '/panolen/src/house/pa6.jpg',
          'title'=>'Căn 3 PN - phòng ngủ master',
          'id' => 5
        ],
        [
          'image' =>  '/panolen/src/house/pa7.jpg',
          'title'=>'Căn 3 PN - phòng ngủ Tây Bắc',
          'id' => 6
        ],
        [
          'image' =>  '/panolen/src/house/pa8.jpg',
          'title'=>'Căn 3 PN - phòng ngủ thường',
          'id' => 7
        ],
        [
          'image' =>  '/panolen/src/house/pa9.jpg',
          'title'=>'ShophouseTTTM',
          'id' => 8
        ],


      ];

      return view('frontend.index')->withDataImage($dataImage);
    }


}

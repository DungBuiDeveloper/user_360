<?php

namespace App\Repositories\Api;
use Cache;

use App\Models\Waranty;
use App\Repositories\BaseRepository;


class WarnrityRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  Category  $model
     */
    public function __construct(Waranty $model)
    {
        $this->model = $model;

    }


    public function getColorAndImageByProductId($id='')
    {
      try {
        $ColorAndImageByProduct = $this->modelProductColorThumb::where('product_id',$id)->with(['getThumbnail' , 'getColor'])->get();
        return $ColorAndImageByProduct;
      } catch (\Exception $e) {
        return $e->getmessage();
      }
    }
}

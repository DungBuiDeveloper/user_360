<?php

namespace App\Repositories\Api;
use Cache;
use App\Models\Event;
use App\Repositories\BaseRepository;

/**
 * Class PermissionRepository.
 */
class EventRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  Category  $model
     */
    public function __construct(Event $model)
    {
        $this->model = $model;
    }

    /**
     * [getAll Get All Cateogry Event].
     * @return [array]       [data event home app]
     */
    public function getWidthCondition($q)
    {
      try {
        return $this->model::where('event_category_id', $q)->with('waranties')->get();
      } catch (\Exception $e) {
        return $e->getmessage();
      }

    }

}

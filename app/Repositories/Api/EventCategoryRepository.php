<?php

namespace App\Repositories\Api;
use Cache;
use App\Models\EventCategory;
use App\Repositories\BaseRepository;

/**
 * Class PermissionRepository.
 */
class EventCategoryRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  Category  $model
     */
    public function __construct(EventCategory $model)
    {
        $this->model = $model;
    }

    /**
     * [getAll Get All Cateogry Event].
     * @return [array]       [data event home app]
     */
    public function getAll()
    {
      try {
        return Cache::get('event_category', $this->model::all());
      } catch (\Exception $e) {
        return $e->getmessage();
      }

    }

}

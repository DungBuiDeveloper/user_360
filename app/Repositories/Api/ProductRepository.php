<?php

namespace App\Repositories\Api;
use Cache;
use App\Models\Product;
use App\Models\ProductColorThumb;
use App\Repositories\BaseRepository;


class ProductRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  Category  $model
     */
    public function __construct(Product $model , ProductColorThumb $ProductColorThumb)
    {
        $this->model = $model;
        $this->modelProductColorThumb = $ProductColorThumb;
    }

    /**
     * [getAll Get All Product].
     * @return [array]       [data event home app]
     */
    public function getWidthCondition($q)
    {
      try {
        $event = $q['event'];
        $productReturn = $this->model::WhereHas('events', function($q) use($event) {
              $q->where('events.id', $event);
          })
          ->where('category_id' , $q['productCategory'])
          ->offset($q['page']*10)->limit(10)->with('getThumbnail')->get();
        foreach ($productReturn as $key => $value) {
          if ($value->getThumbnail) {
            $value->imageThumb = $value->getThumbnail->getUrl();
          }else {
            $value->imageThumb = null;
          }
        }
        return $productReturn;
      } catch (\Exception $e) {
        return $e->getmessage();
      }

    }


    public function getProductById($id='')
    {
      try {
        $product = $this->model::where('id',$id)->with(['getThumbnail' , 'getSmallItem'])->first();
        if ($product->getThumbnail) {
          $product->imageThumb = $product->getThumbnail->getUrl();
        }else {
          $product->imageThumb = null;
        }
      
        return $product;
      } catch (\Exception $e) {
        return $e->getmessage();
      }

    }

    public function getColorAndImageByProductId($id='')
    {
      try {
        $ColorAndImageByProduct = $this->modelProductColorThumb::where('product_id',$id)->with(['getThumbnail' , 'getColor'])->get();
        return $ColorAndImageByProduct;
      } catch (\Exception $e) {
        return $e->getmessage();
      }
    }
}

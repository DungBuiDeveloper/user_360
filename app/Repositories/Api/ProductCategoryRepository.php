<?php

namespace App\Repositories\Api;
use Cache;
use App\Models\Product_category;
use App\Repositories\BaseRepository;

/**
 * Class PermissionRepository.
 */
class ProductCategoryRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  Category  $model
     */
    public function __construct(Product_category $model)
    {
        $this->model = $model;
    }

    /**
     * [getAll Get All Cateogry Event].
     * @return [array]       [data event home app]
     */
    public function getAll()
    {

      try {
        return Cache::get('product_category', $this->model::all());
      } catch (\Exception $e) {
        return $e- getmessage();
      }

    }

}

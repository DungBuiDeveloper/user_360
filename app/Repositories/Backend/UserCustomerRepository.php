<?php

namespace App\Repositories\Backend;

use Cache;
use DataTables;
use App\Models\Auth\User_customer;
use App\Repositories\BaseRepository;

/**
 * Class PermissionRepository.
 */
class UserCustomerRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  Tag  $model
     */
    public function __construct(User_customer $model)
    {
        $this->model = $model;
    }

    public function getAjaxDataTable($search)
    {
        $user_customer = $this->model->get();

        return Datatables::of($user_customer)

            ->editColumn('action', function ($user_customer) {
                $editButton = '<div class="btn-group btn-group-sm"><a
                title="'.__('buttons.general.crud.edit').'"
                href="'.route('admin.user_customers.showFormEdit', $user_customer->id).'"
                class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></a>';

                $viewButton = '<a
                    title="'.__('buttons.general.crud.view').'"
                    href="'.route('admin.user_customers.detail', $user_customer->id).'"
                    class="btn btn-xs btn-warning"><i class="fas fa-eye"></i></a>
                    <a
                    title="'.__('buttons.general.crud.view').'"
                    href="'.route('admin.user_customers.showFormEditChangePass', $user_customer->id).'"
                    class="btn btn-xs btn-success"><i class="fas fa-key"></i>
                    </a>';

                $deleteButton = '<a
                    data-method="delete"
                    data-trans-button-cancel="'.__('buttons.general.cancel').'"
                    data-placement="top"
                    data-toggle="tooltip"
                    href="'.route('admin.user_customers.destroy', ['id' => $user_customer->id]).'"
                    data-id="'.$user_customer->id.'"
                    title="'.__('buttons.general.crud.delete').'"
                    data-original-title="'.__('buttons.general.crud.delete').'"
                    class="btn btn-xs btn-danger btn-delete"><i class="fa fa-trash"></i> </a></div>';

                return $editButton.$viewButton.$deleteButton;
            })
            ->editColumn('name', function ($user) {
                return $user->name;
            })
            ->editColumn('email', function ($user) {
                return $user->email;
            })
            ->rawColumns(['name', 'email', 'action'])
            ->toJson();
    }

    /**
     * [storeCategory Save Category].
     * @param  [type] $data [Post Category Data]
     * @return [type]       [true / false Status Save Model]
     */
    public function storeUserCustomer($data)
    {
        $UserCustomer = $this->model::create($data);

        if ($UserCustomer) {
            return $UserCustomer;
        }

        return false;
    }

    /**
     * [getAlltags].
     * @return [array] [All Category ]
     */
    public function getAlltags()
    {
        try {
            $tag = Cache::rememberForever('tags', function () {
                return $this->model::all();
            });

            return $tag;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * [DELETE Tag].
     */
    public function destroy($id)
    {
        try {
            $deleteUserCustomer = $this->model::find($id);

            if ($deleteUserCustomer) {
                return $deleteUserCustomer->delete();
            }

            return false;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * [getTagBySlug get Detail Tag By Slug].
     * @param  string $slug [description]
     * @return array
     */
    public function getUserCustomerById($id = '')
    {
        try {
            $userCustomer = $this->model::where('id', $id)->with('getThumbnail')->first();

            return $userCustomer;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * [getTagBySlug get Detail Tag By Slug].
     * @param  string $slug [description]
     * @return array
     */
    public function getUserCustomerByEmail($email = '')
    {
        try {
            $userData = $this->model::select('id', 'email', 'name', 'thumbnail')->where('email', $email)->with('getThumbnail')->first();

            if ($userData->thumbnail === null) {
                $userData['thumbnail'] = 'https://fakeimg.pl/430x320/';
                $userData['thumbnail_thumb'] = 'https://fakeimg.pl/250x200/';
                $userData['thumbnail_small'] = 'https://fakeimg.pl/50x50/';
            } else {
                $userData['thumbnail'] = $userData->getThumbnail->getUrl();
                $userData['thumbnail_thumb'] = $userData->getThumbnail->getUrl('thumb');
                $userData['thumbnail_small'] = $userData->getThumbnail->getUrl('small');
            }

            unset($userData['getThumbnail']);

            return $userData;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * [editTag Edit Tag].
     * @param  array $data [data for edit Tag]
     * @return array
     */
    public function editUserCustomer($data)
    {
        try {
            $tagEdit = $this->model::find($data['id']);
            $update = $tagEdit->update($data);

            if ($update) {
                return $tagEdit;
            }

            return false;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}

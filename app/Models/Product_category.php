<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_category extends Model
{
    protected $guarded = [];

    protected $table = 'product_categories';

    protected $fillable = ['name', 'code_color'];



    public function products()
    {
        return $this->belongsTo('App\Models\BackEnd\Product');
    }
}

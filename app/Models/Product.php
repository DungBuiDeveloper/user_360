<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    protected $table = 'products';

    protected $fillable = ['name','price','quantity','name	', 'thumbnail' , 'category_id'];


    public function getThumbnail()
    {
      return $this->belongsTo('App\Models\BackEnd\Media', 'thumbnail');
    }

    public function getSmallItem()
    {
      return $this->belongsToMany('App\Models\Product', 'product_additional', 'product_id', 'additional_id')->with(['getThumbnail']);
    }

    // public function childOf()
    // {
    //     return $this->belongsToMany('App\Models\Category', 'category_parent', 'cat_pa_id', 'cat_id')->withTimestamps();
    // }



    public function events()
    {
        return $this->belongsToMany('App\Models\Event', 'event_product', 'product_id', 'event_id')->withTimestamps();
    }



}

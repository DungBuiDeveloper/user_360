<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductColorThumb extends Model
{
    protected $guarded = [];

    protected $table = 'product_color_thum';

    protected $fillable = ['product_id','color_id','thumbnail_id'];


    public function getThumbnail()
    {
      return $this->belongsTo('App\Models\BackEnd\Media', 'thumbnail_id');
    }

    public function getColor()
    {
      return $this->belongsTo('App\Models\Color', 'color_id');

    }

}

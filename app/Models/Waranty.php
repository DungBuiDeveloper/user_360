<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Waranty extends Model
{
    protected $guarded = [];

    protected $table = 'waranties';

    protected $fillable = ['name', 'price_waranty'];

}

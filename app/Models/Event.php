<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [];

    protected $table = 'events';

    protected $fillable = ['name', 'event_category_id'];

    public function products()
    {
        return $this->belongsToMany('App\Models\product', 'event_product', 'event_id' , 'product_id')->withTimestamps();
    }

    public function waranties()
    {
        return $this->belongsToMany('App\Models\Waranty', 'event_waranty', 'event_id' , 'waranty_id')->withTimestamps();
    }


}

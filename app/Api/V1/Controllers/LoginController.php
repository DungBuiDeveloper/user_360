<?php

namespace App\Api\V1\Controllers;

use Auth;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Repositories\Backend\UserCustomerRepository;


class LoginController extends Controller
{
    public function __construct(UserCustomerRepository $UserCustomerRepository)
    {
        $this->UserCustomerRepository = $UserCustomerRepository;
    }


    protected function guard()
    {
        return Auth::guard('api');
    }

    /**
     * Log the user in.
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {
        $credentials = $request->only(['email', 'password']);

        try {
            $token = Auth::guard('api')->attempt($credentials);

            if (! $token) {
                return response()
                    ->json([
                        'error' => true,
                        'message' => 'The email or password is incorrect.',
                    ], 400);

                // throw new AccessDeniedHttpException();
            }
        } catch (JWTException $e) {
            throw new HttpException(500);
        }


        return response()
            ->json([
                'status' => true,
                'user' => $this->UserCustomerRepository->getUserCustomerByEmail($credentials['email']),
                'token' => $token,
                'expires_in' => Auth::guard('api')->factory()->getTTL() * 60,
            ]);
    }
}

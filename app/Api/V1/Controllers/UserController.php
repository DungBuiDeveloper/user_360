<?php

namespace App\Api\V1\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Backend\UserCustomerRepository;

class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     */
    public function __construct(UserCustomerRepository $UserCustomerRepository)
    {
        $this->middleware('jwt.auth', []);
        $this->UserCustomerRepository = $UserCustomerRepository;
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $email = Auth::guard('api')->user()['email'];

        $user = $this->UserCustomerRepository->getUserCustomerByEmail($email);

        if ($user == false) {
            return response()
                ->json([
                    'error' => true,
                    'message' => 'Error !',
                ], 400);
        }

        return response()->json([
            'status' => true,
            'user' => $user,
        ]);
    }

    // change password
    public function changePassword(Request $request)
    {
        $input = $request->all();
        $userid = Auth::guard('api')->user()->id;

        $rules = [
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        ];
        $validator = Validator::make($input, $rules);

        // error validate
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first(),
                'status' => false,
            ], 200);
        }

        try {
            // check old password
            if ((Hash::check($request->old_password, Auth::guard('api')->user()->password)) == false) {
                return response()->json([
                    'message' => 'Old Password not right!',
                    'status' => false,
                ], 200);
                // check new password diffrent old password
            }
            // create data password
            $data = [
                'id' => $userid,
                'password' => $request->new_password,
            ];
            // edit password
            $edit = $this->UserCustomerRepository->editUserCustomer($data);

            // check update
            if ($edit) {
                // response
                return response()->json([
                    'message' => 'Password updated successfully!',
                    'status' => true,
                ], 200);
            }
            // response
            return response()->json([
                'message' => 'Password updated fails!',
                'status' => false,
            ], 200);

            // catch error
        } catch (\Exception $ex) {
            if (isset($ex->errorInfo[2])) {
                $msg = $ex->errorInfo[2];
            } else {
                $msg = $ex->getMessage();
            }
            // response
            return response()->json([
                'message' => $msg,
                'status' => false,
            ], 200);
        }
    }

    public function renderToken()
    {
        $curl = curl_init();
        $location_id = 'TCWE4P42RYN7X';
        $token = 'EAAAEGDr6aj9kXUvgb4YIvoBhUuP_U7ZJwWEd-eM8KfPOLxOFzcYUk3s2rs88ut9';
        $data = [
            'location_id' => $location_id,
        ];
        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://connect.squareup.com/mobile/authorization-code',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$token,
                'Content-Type: application/json',
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response);

        if (isset($response->authorization_code) && $response->authorization_code) {
            return response()->json([
                'token' => $response->authorization_code,
                'status' => true,
            ], 200);
        }

        return response()->json([
            'message' => $response->message,
            'status' => false,
        ], 200);
    }
}

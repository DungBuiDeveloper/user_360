<?php

namespace App\Api\V1\Controllers;

use App\Models\Auth\User_customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ForgotPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Carbon\Carbon;
class ForgotPasswordController extends Controller
{
  public function RandomString($length) {
    $keys = array_merge(range(0,9), range('a', 'z'));

    $key = "";
    for($i=0; $i < $length; $i++) {
        $key .= $keys[mt_rand(0, count($keys) - 1)];
    }
    return $key;
  }

  public function sendResetEmail(ForgotPasswordRequest $request)
    {
      $user = User_customer::where('email', '=', $request->email)->first();

      if(!$user) {
        return response()->json([
            'message' => 'User Not Found',
            'status' => 'error'
        ], 200);
      }
      $newPass = $this->RandomString(6);

      $user_email = new \stdClass();
      $user_email->email = $request->email;
      $user_email->name = 'Daiwa Space Admin';
      $send = \Mail::send('email.reminder', ['password' => $newPass], function ($m) use ($user_email) {
          $m->to($user_email->email, $user_email->name)->subject('Reset Password');
      });

        $user->password = $newPass;
        $user->save();

        if(!$user) {
          return response()->json([
              'message' => 'User Not Found',
              'status' => 'error'
          ], 200);
        }

        return response()->json([
            'status' => 'ok'
        ], 200);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    private function getPasswordBroker()
    {
        return Password::broker();
    }
}

<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Api\EventCategoryRepository;
use App\Repositories\Api\ProductCategoryRepository;
use App\Repositories\Api\EventRepository;

class HomeController extends Controller
{
  public function __construct(
    EventCategoryRepository $EventCategoryRepository ,
    ProductCategoryRepository $ProductCategoryRepository,
    EventRepository $EventRepository
  )
    {
        $this->EventCategoryRepository = $EventCategoryRepository;
        $this->ProductCategoryRepository = $ProductCategoryRepository;
        $this->EventRepository = $EventRepository;
    }
    //Initial Home
    public function index()
    {

        $eventCategory = $this->EventCategoryRepository->getAll();
        $productCategory = $this->ProductCategoryRepository->getAll();
  

        return response()
            ->json([
                'status' => true,
                'eventCategory' => $eventCategory,
                'productCategory' => $productCategory
            ]);
    }
    //Get Events When Chose Event Category or Some Thing
    public function events(Request $request)
    {
      $condition = $request->all();
      $event = $this->EventRepository->getWidthCondition($condition['eventCategory']);
      return response()
          ->json([
              'status' => true,
              'event' => $event,
          ]);
    }

    public function postExample(Request $request)
    {
        $data = $request->all();

        return response()
            ->json([
                'status' => true,
                'data' => $data,
            ]);
    }




}

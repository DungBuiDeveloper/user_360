<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BackEnd\Product;
use App\Repositories\Api\ProductRepository;


class ProductController extends Controller
{
  public function __construct(
    ProductRepository $ProductRepository
  )
    {
        $this->ProductRepository = $ProductRepository;

    }

    public function index(Request $request)
    {

        $product = $this->ProductRepository->getWidthCondition($request->all());

        return response()
            ->json([
                'status' => true,
                'product' => $product
            ]);
    }
    public function getProductDetail($id ='')
    {

      $product = $this->ProductRepository->getProductById($id);
      $ColorAndImageByProductId = $this->ProductRepository->getColorAndImageByProductId($id);


      if ( !$product ) {
        return response()->json([
            'status' => false,
            'message' => '404 !',
            'data' => $product,
        ]);
      }

      if ($product->getThumbnail != null) {
        $product->thumbnail = env('APP_URL').$product->getThumbnail->getUrl();
      }

      //
      //thumbnail for addition item

      foreach ($product->getSmallItem as $key => $value) {
        if ($value->getThumbnail != null) {
          $value->thumbnail = env('APP_URL').$value->getThumbnail->getUrl();
        }
      }

      $thumbColor = [];
      if (sizeof($ColorAndImageByProductId)) {
        foreach ($ColorAndImageByProductId as $key => $value) {
          $dataArray['thumb'] = $value->getThumbnail->getUrl();
          $dataArray['color'] = $value->getColor->code_color;
          $dataArray['name'] = $value->getColor->name;
          $dataArray['id'] = $value->getColor->id;
          $dataArray['colorLabel'] = ($value->getColor->color == '#fff' || $value->getColor->color == 'white') ? '#000' : '#fff';

          $thumbColor[] = $dataArray;
        }
      }


      $product['thumbColor'] = $thumbColor;



      return response()->json([
          'status' => true,
          'data' => (object)$product,
      ]);

    }

}

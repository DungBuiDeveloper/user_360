<?php

namespace App\Api\V1\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Models\BackEnd\Media;
use App\Http\Controllers\Controller;
use App\Models\BackEnd\MediaLibrary;
use App\Repositories\Backend\MediaRepository;
use App\Http\Requests\BackEnd\MediaLibraryRequest;
use App\Repositories\Backend\UserCustomerRepository;

class MediaLibraryController extends Controller
{
    public function __construct(MediaRepository $MediaRepository , UserCustomerRepository $UserCustomerRepository)
    {
        $this->MediaRepository = $MediaRepository;
        $this->UserCustomerRepository = $UserCustomerRepository;
    }


    /**
     * [storeSignature Create Media Image for upload ].
     * @return [object] [Model iamge]
     */
    public function storeSignature(Request $request)
    {
      $data = $request->all();
        try {
            $file = $data['file'];
            $name = $data['name'];

            $mediaUpload = MediaLibrary::first()
                ->addMediaFromBase64($file)
                ->usingFileName($name.'.png')
                ->usingName($name)
                ->toMediaCollection();

            return response()->json(
              [
                'success' => true,
                'message' => __('media.created'),
                'url_image' => $mediaUpload->getUrl('thumb'),
                'id_image' => $mediaUpload->id
              ]
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function upload_avatar(Request $request)
    {
      $data = $request->all();
        try {
            $file = $data['file'];
            $name = $data['name'];

            $mediaUpload = MediaLibrary::first()
                ->addMediaFromBase64($file)
                ->usingFileName($name.'.png')
                ->usingName($name)
                ->toMediaCollection();
            $userImage = [
              'id'=>$data['userId'],
              'thumbnail' => $mediaUpload->id
            ];
            $editUser = $this->UserCustomerRepository->editUserCustomer($userImage);
            $user = $this->UserCustomerRepository->getUserCustomerByEmail($editUser['email']);

            if ($editUser) {
              return response()->json(
                [
                  'status' => true,
                  'message' => __('media.created'),
                  'user' => $user,
                ]
              );
            }else {
              return response()->json(
                [
                  'status' => false,
                  'message' => 'Update Avatar Fail'
                ]
              );
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}

var panorama1, panorama2, panorama3, viewer, container, infospot;

var bar = document.querySelector( '#bar' );

function onProgressUpdate ( event ) {
  var percentage = event.progress.loaded/ event.progress.total * 100;
  bar.style.width = percentage + "%";
  if (percentage >= 100){
    bar.classList.add( 'hide' );
    setTimeout(function(){
      bar.style.width = 0;
    }, 1000);
  }
}

container = document.querySelector( '#container' );

panorama1 = new PANOLENS.ImagePanorama( 'https://pchen66.github.io/Panolens/examples/asset/textures/equirectangular/tunnel.jpg' );
panorama1.addEventListener( 'progress', onProgressUpdate );

panorama2 = new PANOLENS.ImagePanorama( 'https://pchen66.github.io/Panolens/examples/asset/textures/equirectangular/sunset.jpg' );
panorama2.addEventListener( 'progress', onProgressUpdate );

panorama3 = new PANOLENS.ImagePanorama( 'https://pchen66.github.io/Panolens/examples/asset/textures/equirectangular/road.jpg' );
panorama3.addEventListener( 'progress', onProgressUpdate );

infospot = new PANOLENS.Infospot( 350, PANOLENS.DataImage.Info );
panorama1.add( infospot );

viewer = new PANOLENS.Viewer( { container: container } );
viewer.add( panorama1, panorama2, panorama3 );

// Maunal Set Panorama
var button1 = document.querySelector( '#btn1' );
var button2 = document.querySelector( '#btn2' );
var button3 = document.querySelector( '#btn3' );

// Enter panorama when load completes
function onButtonClick( targetPanorama ) {
  bar.classList.remove( 'hide' );
  viewer.setPanorama( targetPanorama );
}

button1.addEventListener( 'click', onButtonClick.bind( this, panorama1 ) );

button2.addEventListener( 'click', onButtonClick.bind( this, panorama2 ) );

button3.addEventListener( 'click', onButtonClick.bind( this, panorama3 ) );


//Show Hide Simple

$( document ).ready(function() {
  $('.hint').click(function() {
    $(this).fadeOut();
  })


  $('.close_side_bar').click(function() {
    $('.left_site').animate({"left": '-=500'});
    $('.show_side_bar').show();
  });

  $('.show_side_bar').click(function() {
    $('.left_site').animate({"left": '0'});
    $('.show_side_bar').hide();
  });

  $('.cover-item').click(function(event) {
    event.preventDefault();

    $('.cover-item').hide();
    $(this).show();
    $(this).addClass('active');
    $('.cover-item.active .title-class').hide();
    $('.cover-item.active .item-detail').show().animate({"right": '0'},50);
  });
  $('.back').click(function(event) {
    event.stopPropagation();
    $('.cover-item').hide().animate({"right": '150'},1);
    $('.cover-item').removeClass('active').show().animate({"right": '0'},200);
    $('.title-class').show().animate({"right": '0'},50);
    $('.cover-item .item-detail').hide();

  });



});
